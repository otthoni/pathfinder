#ifndef TERKEP_HPP_INCLUDED
#define TERKEP_HPP_INCLUDED
#include<vector>
#include<string>
struct Terkep
{
    std::vector<std::vector<int>> v;
    std::vector<std::vector<bool>> sz;
    int X;
    int Y;
    Terkep(std::string name);
    void rajzol();
};

#endif 
