#include <iostream>
#include "graphics.hpp"
#include"terkep.hpp"
#include"terkepesz.hpp"
#include <vector>
#include <cmath>
#include <algorithm>
#include<climits>
#include<stack>
#include<queue>
#include "terkepesz.hpp"
#include "node.hpp"
using namespace std;
using namespace genv;
// Tesztelő commithoz
Terkepesz::Terkepesz(Terkep tmp) : t(tmp), pontok(t.X, vector<Node*>(t.Y, nullptr)){

        for(int j=0; j<t.Y; j++)
            {
                for(int i=0; i<t.X; i++)
                {
                    bool dry = (t.v[i][j]>=0);
                    pontok[i][j] = new Node(i,j,dry);
                }
            }
    }
    double Terkepesz::distance(Node *one, Node *two){
        return sqrt(pow((two->x-one->x), 2) + pow((two->y-one->y), 2));
    }
    Node* Terkepesz::szomszed(Node *current, int i)
    {
        int new_x = current->x;
        int new_y = current->y;

        if (i == 1 || i == 4 || i == 6) {
            new_x--;
        } else if (i == 3 || i == 5 || i == 8) {
            new_x++;
        }

        if (i == 1 || i == 2 || i == 3) {
            new_y--;
        } else if (i == 6 || i == 7 || i == 8) {
            new_y++;
        }

        if (new_x > 0 && new_x < t.X && new_y > 0 && new_y < t.Y) {
            return pontok[new_x][new_y];
        } else {
           
            return nullptr;
        }
    }
    void Terkepesz::olcso_helyek(Node*& current)
    {
        for (int i = 1; i <= 8; i++)
        {
            Node *Szomszed = szomszed(current, i);
            if (Szomszed && Szomszed->dry)
            {
                double uj_l = current->l + distance(current, Szomszed); //itt jon a matek
                if (uj_l < Szomszed->l || !Szomszed->checked)
                {
                    Szomszed->parent = current;
                    Szomszed->l = uj_l;
                    Szomszed->g = distance(Szomszed, Goal);     
                    if (!Szomszed->checked)
                    {
                        q.push(Szomszed); 

                        //gout << move_to(Szomszed->x, Szomszed->y) << color(0,0,255) << dot << refresh;

                        Szomszed->checked = true; 
                    }
                }
            }
        }
        return;
    }
    void Terkepesz::indulas(Node *&kezdo, Node *&cel)
    {
        Start = pontok[kezdo->x][kezdo->y]; 
        Goal = pontok[cel->x][cel->y]; 
        if (Start == Goal) return;
        Start->l = 0;
        Start->g = distance(Start, Goal);
        Goal->l = INT_MAX;
        Goal->g = 0;
        q.push(Start);
        while (!q.empty())
        {
            Current = q.top();
            q.pop();

            if (Current == Goal)
            {
                rajzol();
                return;
            }
            Current->checked = true;
            olcso_helyek(Current);
        }
        cout << "Nincs száraz lábbal megtehető út." << endl;
    }

    void Terkepesz::update()
    {
        t.rajzol();

        pontok = vector<vector<Node*>>(t.X, vector<Node*>(t.Y, nullptr));

        for (int j = 0; j < t.Y; j++)
        {
            for (int i = 0; i < t.X; i++)
            {
                delete pontok[i][j];
            }
        }

         pontok = vector<vector<Node*>>(t.X, vector<Node*>(t.Y, nullptr));
        for (int j = 0; j < t.Y; j++)
        {
            for (int i = 0; i < t.X; i++)
            {
               Node *a;
                    if(t.v[i][j]>=0) a=new Node(i,j,1);
                    else a=new Node(i,j,0);
                    pontok[i][j]=a;
            }
        }
    }

    void Terkepesz::rajzol()
    {
        int uthossz=0;
        while(Current!=Start)
        {   
            gout << move_to(Current->x, Current->y) << color(255,0,0) << dot;
            Current=Current->parent;
            uthossz++;
        }
        cout << "A megtett út hossza " << uthossz << " egység." << endl;
       
    }

