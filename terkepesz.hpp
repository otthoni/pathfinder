#ifndef TERKEPESZ_HPP_INCLUDED
#define TERKEPESZ_HPP_INCLUDED
#include "node.hpp"
#include "terkep.hpp"
#include<vector>
#include <queue>


struct Terkepesz
{
    Terkep t;
    Node *Current;
    Node *Start;
    Node *Goal;
    std::vector<std::vector<Node*>> pontok;
    std::priority_queue<Node*, std::vector<Node*>, std::greater<Node*>> q; 
    //stack<Node*> q;   //erdekes
    Terkepesz(Terkep tmp);
    double distance(Node *one, Node *two);
    Node* szomszed(Node *current, int i);
    void olcso_helyek(Node*& current);
    void indulas(Node *&kezdo, Node *&cel);
    void update();
    void rajzol();
};
    

#endif 
