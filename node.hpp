#ifndef NODE_HPP_INCLUDED
#define NODE_HPP_INCLUDED
#include<climits>
struct Node
{
    int x, y;
    double l,g;
    bool dry=false;
    bool checked=false;
    Node *parent;
    Node(int _x, int _y, bool sz) : x(_x), y(_y), dry(sz){
        if(dry)
        {
            l=g=INT_MAX;
            checked=false;
        }
        else
        {
            l=g=0;
            checked=true;
        }
    }
    
    bool operator==(const Node& other) const {
        return x == other.x && y == other.y;
    }

    bool operator!=(const Node& other) const {
        return !(*this == other);
    }

    bool operator<(const Node& other) const {
        return g < other.g;
    }
};

    //const: osszehasonlitaskor ne hozzunk letre uj objektumokat 
    //&: a queue Node* tipusokat fog tarolni

#endif
