#include <iostream>
#include "graphics.hpp"
#include "terkep.hpp"
#include <vector>
#include <fstream>
using namespace std;
using namespace genv;

Terkep::Terkep(string name)
    {
        ifstream be(name);
        if (!be.is_open())
            cerr << "hiba" << endl;
        be >> X >> Y;
        int a = X;
        int b = Y;
        v = vector<vector<int>>(X, vector<int>(Y, 0));
        sz= vector<vector<bool>>(X+2, vector<bool>(Y+2, 0));
        for (int j = 0; j < Y; j++)
        {
            for (int i = 0; i < X; i++)
            {
                be >> v[i][j];
            }
        }
    }

    void Terkep::rajzol()
    {
        for (int j = 0; j < Y; j++)
        {
            for (int i = 0; i < X; i++)
            {
                int r, g, b;
                if(v[i][j]>=0){
                    r = 187;
                    g = 218;
                    b = 164;
                }else{
                    r = 74;
                    g = 128;
                    b = 245;
                }
                gout << move_to(i, j) << color(r,g,b) << dot; 
            }
        }
    }


